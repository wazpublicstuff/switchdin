
def convertToKwh(kw, intervalMinutes):  
    # Converts energy flow in kW over an interval to kWh.
    return kw * (intervalMinutes * 60 / 3600)

def tariffCostCalc(energyFlowKwh, costPerKwh):
    # Calculates the tariff cost for the given energy flow in kWh.
    return energyFlowKwh * costPerKwh

def batteryCostCalc(energyFlowKwh, capacityKwh, ratedCycles, replacementCost):
    # Calculates the battery cost based on the energy flow, capacity, rated cycles, and replacement cost.
    degradation = (capacityKwh / abs(energyFlowKwh)) / (ratedCycles / 2) if energyFlowKwh != 0 else 0
    return degradation * replacementCost

def main():
    gridEnergyFlowsKw = [0, 2, -5]  # Energy transferred to/from the grid
    batteryEnergyFlowsKw = [3, 0, 1]  # Energy transferred to/from the battery
    intervalInMinutes = 1
    tariffCostPerKwh = 0.2  # Example tariff cost in $ per kWh
    batteryCapacityKwh = 10  # Example battery capacity in kWh
    ratedCycles = 5000  # Example rated cycles for the battery
    replacementCost = 500  # Example replacement cost in $
    
    totalTariffCost = 0
    totalBatteryCost = 0
    intervalCosts = []
    
    for gridEnergy, batteryEnergy in zip(gridEnergyFlowsKw, batteryEnergyFlowsKw):
        gridEnergyKwh = convertToKwh(gridEnergy, intervalInMinutes)
        batteryEnergyKwh = convertToKwh(batteryEnergy, intervalInMinutes)
        
        # Tariff cost for the interval
        intervalTariffCost = calculateTariffCost(gridEnergyKwh, tariffCostPerKwh)
        
        # Battery cost for the interval
        intervalBatteryCost = calculateBatteryCost(batteryEnergyKwh, batteryCapacityKwh, ratedCycles, replacementCost)
        
        totalTariffCost += intervalTariffCost
        totalBatteryCost += intervalBatteryCost
        
        # Store costs for each interval
        intervalCosts.append((intervalTariffCost, intervalBatteryCost))
    
    totalCost = totalTariffCost + totalBatteryCost
    print("Total Tariff Cost:", totalTariffCost)
    print("Total Battery Cost:", totalBatteryCost)
    print("Total Cost:", totalCost)
    print("Costs per Interval:", intervalCosts)

if __name__ == "__main__":
    main()
